class Perceptron {
private:
    int s{};
    int r{};
    float rate = 0.1;
    float lambda = 0.00001;
    std::vector<float> out;
    std::vector<std::vector<float>> weights;
    std::vector<float> biases;

public:
    Perceptron(int s, int r);

    void setRate(float newRate) {
        rate = newRate;
    }

    void setLambda(float newLambda) {
        lambda = newLambda;
    }

    void train(DataSet dataSet, const DataSetIndexes& indexes) {
        for (int sampleId : indexes.train) {
            std::vector<float> sample = dataSet.normalizedData[sampleId];
            int label = (int)sample[dataSet.columnsCount - 1] - 1;

            // forward propagation
            for (int j = 0; j < r; j++) {
                out[j] = biases[j];
                for (int i = 0; i < dataSet.columnsCount - 1; i++) {
                    out[j] += sample[i] * weights[i][j];
                }
                out[j] = 1 / (1 + (float)exp(-out[j]));
            }

            // backward propagation
            for (int j = 0; j < 3; j++) {
                float expectedOut = (j == label) ?
                                    1 - out.size() * 0.001f :
                                    0.001f;
                float errDelta = expectedOut - out[j];
                float errSignal = errDelta * (out[j] * (1 - out[j]));

                // update weights
                for (int i = 0; i < dataSet.columnsCount - 1; i++) {
                    weights[i][j] += rate * (sample[i] * errSignal + 2 * lambda * weights[i][j]);
                }
                // update bias
                biases[j] += rate * errSignal;
            }
        }
    }
    float test(DataSet dataSet, const DataSetIndexes& indexes) {
        int correct = 0;
        for (int sampleId : indexes.test) {
            std::vector<float> sample = dataSet.normalizedData[sampleId];
            int label = (int)sample[dataSet.columnsCount - 1] - 1;

            // forward propagation
            for (int j = 0; j < r; j++) {
                out[j] = biases[j];
                for (int i = 0; i < dataSet.columnsCount - 1; i++) {
                    out[j] += sample[i] * weights[i][j];
                }
                out[j] = 1 / (1 + (float)exp(-out[j]));
            }

            // what class is it
            int answer = 0;
            for (int j = 0; j < r; j++) {
                if (out[j] > out[answer]) {
                    answer = j;
                }
            }

            if (answer == label) {
                correct++;
            }
        }

        return (float)correct / (float)indexes.test.size();
    }

private:
    void initWeights() {
        std::default_random_engine gen(time(nullptr));
        std::normal_distribution<float> distribution(
                0.0,
                sqrt(2. / (r + s))
        );

        for (int i = 0; i < s; i++) {
            std::vector<float> a;
            weights.push_back(a);
            for (int j = 0; j < r; j++) {
                weights[i].push_back(distribution(gen));
            }
        }

        biases.reserve(r);
        for (int j = 0; j < r; j++) {
            biases.push_back(distribution(gen));
        }
    }
};

Perceptron::Perceptron(int sensorCount, int reactorsCount) {
    s = sensorCount;
    r = reactorsCount;
    out.reserve(r);
    for (int j = 0; j < r; j++) {
        out.push_back(0);
    }
    initWeights();
}
