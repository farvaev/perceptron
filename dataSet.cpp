#include <cmath>
#include <random>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>

class DataSetIndexes {
public:
    std::vector<int> train;
    std::vector<int> test;
};

class DataSet {
public:
    std::vector<std::vector<float>> data;
    std::vector<std::vector<float>> normalizedData;
    int columnsCount{};

    void printData() const {
        return printData(false);
    }

    void printData(bool normalized) const {
        std::vector<std::vector<float>> printingData;
        if (normalized) {
            printingData = normalizedData;
        } else {
            printingData = data;
        }
        for (const std::vector<float>& row : printingData) {
            for (float number : row) {
                std::cout << number << "\t";
            }
            std::cout << "\n";
        }
    }

    void normalize() {
        std::vector<float> mins;
        std::vector<float> maxs;
        for (int i = 0; i < columnsCount - 1; i++) {
            mins.push_back(data[0][i]);
            maxs.push_back(data[0][i]);
        }
        for (auto & sample : data) {
            for (int j = 0; j < sample.size() - 1; j++) {
                if (sample[j] > maxs[j]) maxs[j] = sample[j];
                if (sample[j] < mins[j]) mins[j] = sample[j];
            }
        }
        normalizedData.clear();
        for (auto & sample : data) {
            std::vector<float> normalizedSample;
            normalizedSample.reserve(sample.size());
            for (int j = 0; j < sample.size() - 1; j++) {
                normalizedSample.push_back((sample[j] - mins[j]) / (maxs[j] - mins[j]));
            }
            normalizedSample.push_back(sample.back());
            normalizedData.push_back(normalizedSample);
        }
    }

    DataSetIndexes getIndexes(float trainLength) const {
        auto result = new DataSetIndexes();
        result->test.reserve(data.size());

        for (int i = 0; i < data.size(); i++) {
            result->test.push_back(i);
        }
        std::shuffle(result->test.begin(), result->test.end(), std::mt19937(std::random_device()()));

        int tl = int(trainLength * result->test.size());
        for (int i = 0; i < tl; i++) {
            result->train.push_back(result->test.back());
            result->test.pop_back();
        }

        return *result;
    }
};

DataSet readDataSet(const std::string& filename, int columnsCount) {
    DataSet set = *new DataSet;
    set.columnsCount = columnsCount;
    std::vector<std::vector<float>> data;
    std::vector<int> labels;

    std::ifstream inputFile(filename);

    if (inputFile.good()) {
        float currentNumber;

        while (!inputFile.eof()) {
            std::vector<float> row;
            for (int i = 0; i < columnsCount; i++) {
                inputFile >> currentNumber;
                row.push_back(currentNumber);
            }
            data.push_back(row);
        }

        inputFile.close();

        set.data = data;
    } else {
        std::cout << "Error!";
        exit(0);
    }

    return set;
}