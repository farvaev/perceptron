#include <iostream>

#include "dataSet.cpp"
#include "perceptron.cpp"

int main() {
    DataSet dataSet = readDataSet("../datasets/seeds/seeds_dataset.txt", 8);
    DataSetIndexes indexes = dataSet.getIndexes(0.8);

    dataSet.normalize();

    auto *perceptron = new Perceptron(7, 3);

    int epochs = 50;

    perceptron->setRate(0.15);
    perceptron->setLambda(0.00001);

    for (int ep = 0; ep < epochs; ep++) {
        perceptron->train(dataSet, indexes);
    }

    float percent = perceptron->test(dataSet, indexes) * 100;
    std::cout << percent << "%\n";

    return 0;
}
